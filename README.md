# Neubox challenge

_ consta de 4 secciones:
* decodificador de mensajes
* verificador de ganador 
* codigo de algoritmo de verificador de ganador en dart
* portafolio

## Comenzando 🚀

_El sistema esta echo en su gran mayoria de codigo nativo en php sin la utilizacion de algun framework 

_Es necesario tener un apache o un servidor web y acceder al index del la carpeta raiz

_Es necesario tener php < 7.*


### Pre-requisitos 📋

_El sistema cotiene escritura y lectura de archivos es nesesario darle permisos ala carpeta

```
$> chmod 755 neubox_challenge
```

### Instalación 🔧

_ clonar el proyecto o descargarlos 

_ colocarlo en un entorno apache o un servidor web 

- correr composer

```
$> composer update
```

- correr unit test

```
$> ./vendor/bin/phpunit src/tests
```

_ accerder al index.php


## Construido con 🛠️

* php - lenguaje base
* boostrap -  base de style 

## Autor ✒️

* **Orlando martinez** - [linkedin](https://www.linkedin.com/in/orlando-martinez-b2513a167/)
