<?php
include_once 'src/Page.php';
require_once('src/service/AdminFile.php');
require_once('src/service/DecoderNeubox.php');


class Index extends Pagina{

    private  $_msjError;
    private  $_msjOk;
    private  $_fileArrar;
    private  $_isDowloadFile;
    function __construct() {
        $this->_isDowloadFile = false;
        $this->_msjError = '';
        if(!empty($_FILES['archivo']))
            $this->init();

        parent::head(1);
        $this->body();
        parent::footer();
    }


    public function init(){
        try {
            AdminFile::subirFile($_FILES['archivo']);
            $this->validFile();
            $this->generateResult();
        } catch (Exception $e) {
            $this->_msjError = $e->getMessage();
        }
    }


    private function generateResult(){
        $service  = new DecoderNeubox(trim($this->_fileArrar[3]));
        $vUno = $service->isValid(trim($this->_fileArrar[1]));
        $vDos = $service->isValid(trim($this->_fileArrar[2]));
        $result = [$vUno?"SI":"NO", $vDos?"SI":"NO"];
        if($vUno || $vDos){
            if($vUno && $vDos){
                $this->_msjError = "Existen 2 mensajes en un mismo codigo";
            }else{
                $this->_msjOk = "La el msj valido es la linea: Mensaje 1: $result[0], Mensaje 2: $result[1].";
            }
        }else{
            $this->_msjError = "No se encontro mensaje";
        }

        AdminFile::createFile($result);
        $this->_isDowloadFile = true;
    }

    private function validFile(){
        $this->_fileArrar = AdminFile::getFile();
        $lineaOne = explode(" ", $this->_fileArrar[0]);
        $lineaTwo =  $this->_fileArrar[1];
        $lineaThree =  $this->_fileArrar[2];
        $lineaFour =  $this->_fileArrar[3];

        if(count($lineaOne) !== 3)
            throw new Exception('El formato de txt no es el correcto: linea 1');

        if((int)$lineaOne[0]  <= 2 || 50 <= (int)$lineaOne[0] )
            throw new Exception('El formato de txt no es el correcto: linea 1 el rango debe ser de 2 a 50');

        if((int)$lineaOne[1]  <= 2 || 50 <= (int)$lineaOne[1] )
            throw new Exception('El formato de txt no es el correcto: linea 1 el rango debe ser de 2 a 50');

        if((int)$lineaOne[2]  <= 3 || 5000 <= (int)$lineaOne[2] )
            throw new Exception('El formato de txt no es el correcto: linea 1 el rango debe ser de 3 a 5000');

        if((int)$lineaOne[2]  <= 3 || 5000 <= (int)$lineaOne[2] )
            throw new Exception('El formato de txt no es el correcto: linea 1 el rango debe ser de 3 a 5000');

        if((int)$lineaOne[0] !== strlen(trim($lineaTwo)))
            throw new Exception('El formato de txt no es el correcto: linea 2 no coinciden con los numeros de caracteres linea 1');

        if((int)$lineaOne[1] !== strlen(trim($lineaThree)))
            throw new Exception('El formato de txt no es el correcto: linea 3 no coinciden con los numeros de caracteres linea 1');

        if((int)$lineaOne[2] !== strlen(trim($lineaFour)))
            throw new Exception('El formato de txt no es el correcto: linea 4 no coinciden con los numeros de caracteres linea 1');

        if(!preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $lineaFour))
            throw new Exception('El formato de txt no es el correcto: linea 4 no contiene caracteres no permitidios');

        if(preg_match('/(.)\1/m', $lineaTwo))
            throw new Exception('El formato de txt no es el correcto: linea 2 contiene caracteres repetidos');

        if(preg_match('/(.)\1/m', $lineaThree))
            throw new Exception('El formato de txt no es el correcto: linea 3 contiene caracteres repetidos');

    }


    public function body() {
        parent::showMessengeError($this->_msjError);
        parent::showMessengeSuccess($this->_msjOk);
      echo
      '<div class="card text-center">
          <div class="card-header">
            Decodificador neubox
          </div>
          <div class="card-body">
            <p class="card-text">Por favor ingrese un archivo txt con el formato correspondiente.</p>
              <form action="index.php" method="post" enctype="multipart/form-data">
      
                <br>
                <b>Subir archivo a decodificar: </b>
                <br>
                <input name="archivo" type="file">
                <br>
                <input type="submit" class="btn btn-primary" value="Decodificar">
              </form>
          </div>
          <div class="card-footer text-muted">
            '.(!$this->_isDowloadFile ? 'Sin archivo' : '<a href="output.txt" target="_blank" class="btn btn-success btn-lg">Archivo</a>').'
          </div>
        </div>';
    }

}


$objCarte = new Index();