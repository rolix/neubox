<?php
include_once './src/Page.php';
require_once('./src/service/AdminFile.php');
require_once('./src/service/CalculateWinner.php');


class Portafolio extends Pagina{

    private  $_msjError;
    private  $_msjOk;

    function __construct() {
        $this->_msjError = '';
        $this->_msjOk = '';
        parent::head(4);
        $this->body();
        parent::footer();
    }

    public function body() {
      echo
      '<div class="card text-center">
          <div class="card-header">
            Portafolio
          </div>
          <div class="card-body text-center" >
            <p class="card-text">Este es beter el ultimo proyecto en el que participe, yo fui el encargado del desarrollo de esta plataforma en su totalidad.
            <br>
            Me gustaría resaltar las creaciones de interfaces fueron desarrolladas en VUE
            <br>
            les facilito acceso a la plataforma para que tengan acercamiento a parte de mi trabajo
            </p>
 
            <ul>
            <li style="list-style: none">Usuario: ing.orlando.mar@gmail.com</li>
            <li style="list-style: none">password: orlando19</li>
            </ul>
            <div id="carouselExampleCaptions" class="carousel slide mx-auto" data-ride="carousel" style=" width: 600px !important; height: 400px">
            <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="./assets/img5.png" class="d-block w-100"  alt="...">
            </div>
            <div class="carousel-item">
              <img src="./assets/img2.png" class="d-block w-100" style="width: 300px" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./assets/img1.png" class="d-block w-100" style="width: 300px" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./assets/img3.png" class="d-block w-100" style="width: 300px" alt="...">
            </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev" >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only" style="background: black">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next" >
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only" style="background: cadetblue; color: black">Next</span>
            </a>
            </div>
          </div>
          <div class="card-footer text-muted">
            <a href="https://beterapp.com" target="_blank" class="btn btn-success btn-lg">Ir a sitio</a>
          </div>
        </div>';
    }

}


$objCarte = new Portafolio();