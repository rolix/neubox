import 'dart:math';

class CalculateWinner{
  List<String> rounds ;
  List<dynamic> roundValued ;
  CalculateWinner(this.rounds) {
    mapRound();
  }

//  este metodo es mas practico en php en este lenguaje hay mas herramientas que optimizarian este metodo
  List<dynamic>winner(){
    List<dynamic> columnDiff = roundValued.map((e) => e[1]).toList();
    int maxValue = columnDiff.reduce((curr, next) => curr > next? curr: next);
    List<dynamic> maxWinner = roundValued.firstWhere((element) => element[1] == maxValue);
    if(isTiedScore(maxWinner)){
      maxWinner = recalculateWinner(maxWinner);
    }

    return maxWinner;
  }


  List<dynamic> recalculateWinner(maxWinner){
    roundValued = roundValued.where((element) => element[1] != maxWinner[1]).toList();
    return winner();
  }

  bool isTiedScore(maxWinner){
    bool _tied = false;
    List<dynamic> repeatScore = roundValued.where((element) => element[1] == maxWinner[1]).toList();
    if(repeatScore.length != 1){
      return repeatScore.map((e) => e[0]).toList().contains(1) && repeatScore.map((e) => e[0]).toList().contains(2)  ? true : false;
    }
    return _tied;
  }


  void mapRound(){
    int sumPlayerOne = 0;
    int sumPlayerTwo = 0;

    roundValued = rounds.map((item){
      sumPlayerOne = sumPlayerOne + int.parse(item.split(" ")[0]);
      sumPlayerTwo = sumPlayerTwo + int.parse(item.split(" ")[1]);
      return [sumPlayerOne > sumPlayerTwo ? 1 : 2, (sumPlayerOne - sumPlayerTwo).abs()];
    }).toList();
  }


}