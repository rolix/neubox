<?php

use PHPUnit\Framework\TestCase;
require_once('src/service/DecoderNeubox.php');
require_once('src/service/CalculateWinner.php');

final class TestNeubox extends TestCase
{

    public function testDecoderNeubox()
    {
        $cadena = 'XXcaaamakkCCessseAAllFueeegooDLLKmmNNN';
        $llave = 'CeseAlFuego';
        $service  = new DecoderNeubox($cadena);
        $this->assertTrue($service->isValid($llave));
    }

    public function testGameRound()
    {
        $rounds = ['140 82', '89 134', '90 110', '112 106', '88 90'];
        $result = mainCalculateWinner($rounds);
        $this->assertIsArray($result);
    }

}

