<?php


class AdminFile
{

    static public function subirFile($file){
        $nombre_archivo = 'archivo.txt';
        if ($file['type'] !== 'text/plain') {
            throw new Exception('El formato no es un txt');
        }else{
            if (move_uploaded_file($file['tmp_name'],  $nombre_archivo)){
              return true;
            }else{
                throw new Exception('Algo salio mal al cargar el archivo');
            }
        }
    }

    static public function getFile()
    {
        $archivo = fopen("archivo.txt", "r");
        $lineas = array();
        while (!feof($archivo)) {
            // Leyendo una linea
            $traer = fgets($archivo);
            if(!empty($traer))
                array_push($lineas, $traer);
        }
        fclose($archivo);
        return $lineas;
    }

    static public function createFile($print){
        $myfile = fopen("output.txt", "w") or die("Unable to open file!");
        foreach ($print as $value) {
            fwrite($myfile, "$value \n");
        }
        fclose($myfile);
    }
}