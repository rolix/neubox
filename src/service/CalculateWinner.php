<?php

require_once('src/service/DecoderNeubox.php');

//metodo que calcula el ganador buscando la mayor diferencia
function calculateWinner($round)
{
    $arrayColumnRound = array_column($round, 1);
    $maxWinner = $round[array_search(max($arrayColumnRound), $arrayColumnRound)];
    if (isTiedScore($round, $maxWinner)) {
        $maxWinner = recalculateWinner($round, $maxWinner);
    }
    return $maxWinner;
}

//metodo que en  caso  de empate vuelve a buscar el siguiente max diferrencia
function recalculateWinner($round, $maxWinner)
{
    $round = array_filter($round, function ($data) use ($maxWinner) {
        return $data[1] !== $maxWinner[1];
    });
    return calculateWinner(array_values($round));
}


//validacion si existe un empate
function isTiedScore($round, $maxWinner)
{
    $_tied = false;
    $repeatScore = array_filter($round, function ($data) use ($maxWinner) {
        return $data[1] == $maxWinner[1];
    });
    if (count($repeatScore) !== 1) {
        $playersWin = array_unique(array_column($repeatScore, 0));
        $_tied = count($playersWin) !== 1 ? true : false;
    }
    return $_tied;
}

function isValidRound($arrayRounds){
    $isValid = true  ;
    foreach ($arrayRounds as $value) {
        if(!preg_match('/^[0-9 ]*$/', $value)) {
            $isValid = false;
            break ;
        }
    }
    return $isValid;
}

//mapea el array y crea la  suma y los ganadores por ronda
function mapRound($round)
{
    $sumPlayerOne = 0;
    $sumPlayerTwo = 0;
    $newRounds = [];
    foreach ($round as $itemtemValue) {
        $players = explode(" ", $itemtemValue);
        $sumPlayerOne = $sumPlayerOne + (int)$players[0];
        $sumPlayerTwo = $sumPlayerTwo + (int)$players[1];
        $diff = $sumPlayerOne - $sumPlayerTwo;
        $playerWinnRound = $sumPlayerOne > $sumPlayerTwo ? 1 : 2;
        array_push($newRounds, [$playerWinnRound, abs($diff)]);
    }
    return $newRounds;
}

//inicio de methodo de verificacion
function mainCalculateWinner($lineas)
{
    $round = mapRound($lineas);
    return calculateWinner($round);
}
