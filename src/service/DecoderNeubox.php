<?php

/**
 * Class DecoderNeubox
 * classse encargada de validar si existe un  codigo de caracteres en una cadena
 * la funcionalidad del algoritmo es encontrar el inicio de la cadena que tenga el mensaje a evaluar si no pasa la evaluacion
 * es removida y sigue con su proceso de busqueda asta terminar toda la cadena
 */
class DecoderNeubox {

    Private $_cadena;
    Private $_shortCadena;
    Private $_valid;

    function __construct($cadena) {
        $this->_cadena = str_split($cadena);
    }

/*
 * @var $key String contiene la cadena a evaluar
 * */
    function isValid($key){
        $this->_valid = false;
        $keyArray = str_split($key);
        $this->_shortCadena = $this->_cadena;
        while ($this->recursiveCheck($keyArray)) {
            $this->evaluateCadena($keyArray);
        }
        return $this->_valid;
    }

/*
 * @var $keyArray array de la cadena a evaluar
 * metodo que busca el inicio de coincidencias para empezar a evaluar
 * */
    private function recursiveCheck($keyArray){
        if(empty(array_search($keyArray[0], $this->_shortCadena))){
            return false;
        }else{
            $this->cutCadena($keyArray[0]);
            return true;
        }
    }

/*
 * @var $keyArray array de la cadena a evaluar
 * metodo que se encarga de evaluacion de la cadena
 * */
    private function evaluateCadena($keyArray){
        $posicionEvaluar = 0;
        $this->_valid = false;

        foreach($this->_shortCadena as $key => $letter) {

            if($keyArray[$posicionEvaluar] == $letter){
                if(count($keyArray)-1 == $posicionEvaluar){
                    $this->_shortCadena = [];
                    $this->_valid = true;
                    break;
                }
                $posicionEvaluar++;
                continue;
            }
            elseif ($keyArray[$posicionEvaluar - 1] == $letter ){
                continue;
            }else{
                $this->_valid = false;
                $this->cutCadena($letter);
                break;
            }
        }
    }

    private function cutCadena($position){
        $this->_shortCadena = array_slice($this->_shortCadena, array_search($position, $this->_shortCadena));
    }

}

