<?php

abstract class Pagina{

    public function head($index){
        $salida = '
    <!doctype html>
    <html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <title>Challenge  neubox</title>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" ref="./index.php">Neubox</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item '.($index == 1 ? 'active' : '').'">
                    <a class="nav-link" href="./index.php">Problema 1 <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item '.($index == 2 ? 'active' : '').'">
                    <a class="nav-link" href="./page2.php">Problema 2</a>
                </li>
                <li class="nav-item '.($index == 3 ? 'active' : '').'">
                    <a class="nav-link"  href="./page3.php" >Nuevo lenguaje</a>
                </li>
                <li class="nav-item '.($index == 4 ? 'active' : '').'">
                    <a class="nav-link"  href="./page4.php">Portafolio</a>
                </li>
            </ul>
        </div>
    </nav>
    ';
        echo $salida;
    }

    protected abstract function body();

    public function footer(){
        $salida = '
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
            </body>
            </html>
        ';
        echo $salida;
    }

    public function showMessengeSuccess($success){
        if(!empty($success)){
            echo' 
            <div class="alert alert-success" role="alert">
              '.$success.'
            </div>
            ';
        }
    }

    public function showMessengeError($error){
        if(!empty($error)){
            echo' 
            <div class="alert alert-danger" role="alert">
              '.$error.'
            </div>
            ';
        }
    }

}

