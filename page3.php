<?php
include_once './src/Page.php';
require_once('./src/service/AdminFile.php');
require_once('./src/service/CalculateWinner.php');


class Marcadores extends Pagina{

    private  $_msjError;
    private  $_msjOk;
    private  $_fileArrar;
    private  $_lineaOne;
    private  $_rounds;


    function __construct() {
        $this->_msjError = '';
        $this->_msjOk = '';
        parent::head(3);
        $this->body();
        parent::footer();
    }


    public function init(){
        try {
            AdminFile::subirFile($_FILES['archivo']);
            $this->validFile();
            $this->generateResult();
        } catch (Exception $e) {
            $this->_msjError = $e->getMessage();
        }
    }




    public function body() {
      echo
      '<div class="card text-center">
          <div class="card-header">
            Code in dart
          </div>
          <div class="card-body">
            <p class="card-text">Se realizo el mismo metodo de verificar ganador de un jugador en codigo dart.</p>
          </div>
          <div class="card-footer text-muted">
            <a href="calculate_winner.dart" target="_blank" class="btn btn-success btn-lg">Ver codigo</a>
          </div>
        </div>';
    }

}


$objCarte = new Marcadores();