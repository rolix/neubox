<?php
include_once './src/Page.php';
require_once('./src/service/AdminFile.php');
require_once('./src/service/CalculateWinner.php');


class Marcadores extends Pagina{

    private  $_msjError;
    private  $_msjOk;
    private  $_fileArrar;
    private  $_lineaOne;
    private  $_rounds;
    private  $_isDowloadFile;

    function __construct() {
        $this->_isDowloadFile = false;
        $this->_msjError = '';
        if(!empty($_FILES['archivo']))
            $this->init();

        parent::head(2);
        $this->body();
        parent::footer();
    }


    public function init(){
        try {
            AdminFile::subirFile($_FILES['archivo']);
            $this->validFile();
            $this->generateResult();
        } catch (Exception $e) {
            $this->_msjError = $e->getMessage();
        }
    }


    private function generateResult(){
        $result = mainCalculateWinner($this->_rounds);
        if(!empty($result)){
            $this->_msjOk = "El ganador fue jugador $result[0] con diferencia de $result[1]";
        }else{
            $this->_msjError = 'No hay ganador';
        }
        AdminFile::createFile([implode(" ", $result)]);
        $this->_isDowloadFile = true;
    }

    private function validFile(){
        $this->_fileArrar = AdminFile::getFile();
        $this->_lineaOne = (int)$this->_fileArrar[0];
        $this->_rounds = array_slice($this->_fileArrar, 1);

        if((int)$this->_lineaOne  <= 0 || 10000 <= (int)$this->_lineaOne )
            throw new Exception('El formato de txt no es el correcto: linea 1 el rango debe ser de 0 a 10000');

        if((int)$this->_lineaOne !== count($this->_rounds))
            throw new Exception('El formato de txt no es el correcto: linea 1 no coinciden con las vueltas');

        if(!isValidRound($this->_rounds))
            throw new Exception('Una ronda no tiene el formato correcto');

    }

    public function body() {
        parent::showMessengeError($this->_msjError);
        parent::showMessengeSuccess($this->_msjOk);
      echo
      '<div class="card text-center">
          <div class="card-header">
            Verificador de ganador
          </div>
          <div class="card-body">
            <p class="card-text">Por favor ingrese un archivo txt con el formato correspondiente.</p>
            <p class="card-text">El criterio de empate: maxima diferencia del anterior valor del empate.</p>
              <form action="page2.php" method="post" enctype="multipart/form-data">
      
                <br>
                <b>Subir archivo a verificar: </b>
                <br>
                <input name="archivo" type="file">
                <br>
                <input type="submit" class="btn btn-primary" value="Verificar">
              </form>
          </div>
          <div class="card-footer text-muted">
            ' .(!$this->_isDowloadFile ? 'Sin archivo' : '<a href="output.txt" target="_blank" class="btn btn-success btn-lg">Archivo</a>').'
          </div>
        </div>';
    }

}


$objCarte = new Marcadores();